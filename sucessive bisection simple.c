#include<stdio.h>

float e,x1,x2,xmid;
float func(float x)
{
  float fx;
  fx = x*x*x -4*x -9;
  return fx;
}

int main()
{
  printf("\t\t Program to demonstrate sucessive bisection\n");
  printf("\t\t f(x) = x^2 - 36\n");
  printf("Enter the interval of search\n");
  printf("Lower value = ");
  scanf("%f",&x1);
  printf("Upper value = ");
  scanf("%f", &x2);
  printf("\n x \t\t f(x)\n");
  printf("__________________________\n\n");
  xmid = (x1 + x2) /2;
   while( func(xmid) !=0)
   {
      if(func(xmid) <0)
	{
	  x1= xmid;
	  x2=x2;
	  printf("f(%f) = %f\n",xmid, func(xmid));
	}
      else
	{ 
	  x1=x1;
	  x2=xmid;
	 printf("f(%f) = %f\n",xmid, func(xmid));
	}
      xmid = (x1 +x2)/2;
    }
     
      if(func(xmid) ==0) 
	{
	  printf("f(%f) = %f \t Root found is %f", xmid,func(xmid),xmid);
	}
      else {
	printf("\n");
      }


  return 0;
}
