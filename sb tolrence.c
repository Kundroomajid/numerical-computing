#include<stdio.h>
#include<math.h>
float fx, x1,x2,xmid,err;

float func(float x)
{
  fx = x *x *x -4*x -9;
  return fx;
}

void main()
{

  printf("\t\t Program to demonstrate sucessive bisection\n");
  printf("\t\t f( x ) = x^3 -4x -9\n");
  printf("Enter the interval of search\n");
  printf("Lower value = ");
  scanf("%f",&x1);
  printf("Upper value = ");
  scanf("%f", &x2);
  printf("Enter the error allowed = ");
  scanf("%f",&err);
  printf("\n x \t\t f(x)\n");
  printf("__________________________\n\n");
 
  do 
    {
    xmid = (x1 + x2) /2;
    if(func(x1) * func(xmid) < 0)
      {
      x2=xmid;
     printf("f(%f) = %f\n",xmid, func(xmid));
      }
    else
      {
	
	  x1 = xmid;
    printf("f(%f) = %f\n",xmid, func(xmid));
      }
    }	
    while(fabs((float)(x1-x2)) > err);
    printf("%f = approx root of given function ",xmid);
    }
